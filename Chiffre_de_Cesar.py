from tkinter.messagebox import *
from tkinter import * 
from Fonction import cesar_crypt,cesar_decrypt,cesar_crypt_circu,cesar_decrypt_circu

def button_cryptage():
    textarea_crypt.config(text='' + cesar_crypt(value_cryptage.get(),val_cle.get()))
    input_crypt.delete(0, 'end')

def button_decryptage():
    textarea_decrypt.config(text=''+ cesar_decrypt(value_decryptage.get(),val_cle.get()))
    input_decrypt.delete(0, 'end')

def button_cryptage_circulaire():
    textarea_crypt.config(text='' + cesar_crypt_circu(value_cryptage.get(),val_cle.get()))
    input_crypt.delete(0, 'end')

def button_decryptage_circulaire():
    textarea_decrypt.config(text=''+ cesar_decrypt_circu(value_decryptage.get(),val_cle.get()))
    input_decrypt.delete(0, 'end')
 
root = Tk()

root.minsize(500, 150)
root.rowconfigure(0, weight=1)
root.columnconfigure(1, weight=1)
root.title("Chiffrement / Déchiffrement Le chiffre de César")
root.resizable(width=True, height=True)


label_title_crypt = Label(root, text="Chiffrement")
label_title_crypt.grid(column=0, row=0)

label_title_decrypt = Label(root, text="Déchiffrement")
label_title_decrypt.grid(column=2, row=0)

value_cryptage = StringVar()
input_crypt = Entry(root, textvariable=value_cryptage)
input_crypt.grid(column=0, row=1, sticky='nsew')

value_decryptage = StringVar()
input_decrypt = Entry(root, textvariable=value_decryptage)
input_decrypt.grid(column=2, row=1, sticky='nsew')

crypt_button=Button(root, text="Crypter", command=button_cryptage)
crypt_button.grid(column=0 , row=2 )

decrypt_button=Button(root, text="Décrypter", command=button_decryptage)
decrypt_button.grid(column=2 , row=2 )

crypt_button=Button(root, text="Crypter Circulaire", command=button_cryptage_circulaire)
crypt_button.grid(column=0 , row=4 )

decrypt_button=Button(root, text="Décrypter Circulaire", command=button_decryptage_circulaire)
decrypt_button.grid(column=2 , row=4 )

text_cle = Label(root, text="Clé de décalement")
text_cle.grid(column=1, row=1)

val_cle = IntVar()
val_cle.set(3)
input_cle = Spinbox(root,from_=1,to=20,increment=1,textvariable=val_cle,width=8)
input_cle.grid(column=1, row=2)

textarea_crypt = Label(root, text=" ")
textarea_crypt.grid(column=0, row=3)

textarea_decrypt = Label(root, text=" ")
textarea_decrypt.grid(column=2, row=3)

exit_button=Button(root, text="Fermer la fenêtre", command=root.quit)
exit_button.grid(column=1 , row= 4)

root.mainloop()