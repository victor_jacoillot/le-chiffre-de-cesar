def cesar_crypt(str_a_crypter,cle):
    res = ''
    for charac in str_a_crypter:
        res=res+chr(ord(charac)+cle)
    return res

def cesar_decrypt(str_a_decrypter,cle):
    res = ''
    for charac in str_a_decrypter:
        res=res+chr(ord(charac)-cle)
    return res

def cesar_crypt_circu(str_a_crypter,cle):
    res = ''
    for charac in str_a_crypter:
        if ord(charac)+cle>122 and ord(charac)+cle>90:
            res=res+chr(ord(charac)-26+cle)
        else : res=res+chr(ord(charac)+cle)
    return res

def cesar_decrypt_circu(str_a_decrypter,cle):
    res = ''
    for charac in str_a_decrypter:
        if ord(charac)-cle<97 or ord(charac)-cle<65:
            res=res+chr(ord(charac)+26-cle)
        else : res=res+chr(ord(charac)-cle)   
    return res
