import unittest 

from Fonction import cesar_crypt,cesar_crypt_circu,cesar_decrypt,cesar_decrypt_circu

class TestCesarFunctions(unittest.TestCase):

     def test_crypt(self):
        self.assertEqual(cesar_crypt("abc",3), "def")
        self.assertEqual(cesar_crypt_circu("abcz",3), "defc")

     def test_decrypt(self):
        self.assertEqual(cesar_decrypt("def",3), "abc")